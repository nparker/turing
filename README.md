# THE TURINATOR
### A Turing machine simulator application

## Background
A Turing machine is an abstract mathematical model of a computer. It recognizes a language, uses strings as inputs, and based on the language will either accept that string, reject the string, or enter an infinite loop and never provide an answer.  A Turing machine is represented by a “tape” and “tape head.”  The tape is composed of cells, each of which contain one character, and is infinitely long in one direction.  The tape head is a finite control element that points to a single cell on the tape, keeps track of the state of the machine, can read a character from a cell or write a character to a cell, and can move one cell to the left or one cell to the right.  A set of states, alphabets, and transition functions are used to define an algorithm or “language” which the Turing machine uses to accept or reject input strings.

## Overview
The purpose of this application is to simulate a Turing machine, allowing the user to define the components of a Turing machine, a set of input strings to run the Turing machine on, and interactively trace through the operation.  The application is a command-line application and has no graphical user interface

## Getting Started

### Prerequisites

The only prerequisite is that Make and the G++ compiler version 5 or above is installed. This application has been tested on linux-based systems as well as WSL (Windows Subsystem for Linux).

### Compilation

Building the application is simple:

```
$ make
```

### Startup

The application accepts a single command-line argument, that is the name of the Turing machine to be run, not including any file extensions.

Example:
```
$ ./tm anbn
```

## Authors
* Neil Parker

## License
This project is licensed under the MIT License - see [LICENSE.md](LICENSE.md) for details