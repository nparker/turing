TARGET=bin/tm
CC=g++
CFLAGS=-std=c++11 -g

SRCDIR=src
BUILDDIR=build
SRCEXT=cpp
SOURCES=$(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS=$(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))

.PHONY: all clean test print

default: $(TARGET)

$(TARGET): $(OBJECTS)
	@echo "Linking..."
	@mkdir -p bin
	@$(CC) $(CFLAGS) -o $@ $^

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT)
	@echo "Compiling $@..."
	@mkdir -p $(BUILDDIR)
	@$(CC) $(CFLAGS) -c -o $@ $<

clean:
	@echo "Cleaning..."
	@rm -rf $(BUILDDIR) $(TARGET) src.pdf

test: $(TARGET)
	./$(TARGET) anbn

print:
	@echo "Writing output to src.pdf..."
	@find src -name "*.cpp" -o -name "*.hpp" | xargs enscript -C -T4 -Ecpp -M Letter -fCourier8 -o - | ps2pdf - src.pdf
	@echo "Done!"
