#include "tm_inputstrings.hpp"

void TuringMachineInputStrings::load(string name, const TuringMachine& tm)
{
	filename = name;

	ifstream str(filename + ".str");

	string def;

	if(str.is_open())
	{
		stringstream ss;
		ss << str.rdbuf();
		str.close();

		// this is to deal with different line endings in linux vs windows
		def = regex_replace(ss.str(), regex("[\\r\\n]+"), "\n");
	}

	vector<string> strs = util::split(def);

	bool discarded = false;
	for(auto it = strs.begin(); it != strs.end(); ++it)
		if(tm.isValidInputString(*it))
			insertInputString(*it);
		else
			discarded = true;

	changed = discarded;
}

// validates that index is valid
bool TuringMachineInputStrings::findInputString(int index, string& input_string) const
{
	if(index > 0 && index <= input_strings.size())
	{
		input_string = input_strings[index - 1];
		return true;
	}
	return false;
}

// validates that strings are not duplicate
// returns true if successfully inserted
bool TuringMachineInputStrings::insertInputString(string str)
{ // str is as it appears in file or user input
	// skip empty lines
	if(str == "")
		return false;

	// backslash on a line by itself represents the empty string
	if(str == "\\")
		str = "";

	// check for duplicate input string
	for(auto it = input_strings.begin(); it != input_strings.end(); ++it)
	{
		if(str == *it)
		{
			cout << "Discarding duplicate input string '" << str << "'..." << endl;
			return false;
		}
	}

	input_strings.push_back(str);
	changed = true;

	return true;
}

void TuringMachineInputStrings::removeInputString(int index)
{
	string input;

	if(findInputString(index, input))
	{
		input_strings.erase(input_strings.begin() + (index - 1));
		changed = true;
		if(input == "") input = "\\";
		cout << "Input string '" << input << "' removed!" << endl;
	}
	else
		cout << "Invalid input string number!" << endl;
}


void TuringMachineInputStrings::viewInputStrings() const
{
	if(input_strings.size() == 0)
		cout << "Input string list is empty!" << endl;

	for(int i = 1; i <= input_strings.size(); i++)
	{
		string str = input_strings[i - 1];
		
		if(str == "") str = "\\";

		cout << " " << i << ". " << str << endl;
	}
}

void TuringMachineInputStrings::writeInputStrings() const
{
	if(!changed)
		return;

	ofstream str(filename + ".str");
	
	if(str.is_open())
	{
		for(auto it = input_strings.begin(); it != input_strings.end(); ++it)
		{
			if(it != input_strings.begin())
				str << endl;

			string line = *it;
			if(line == "") line = "\\";

			str << line;
		}
		
		str.close();

		cout << "Input string file written successfully!" << endl;
	}
	else
		cout << "Could not write input string file!" << endl;
}
