#pragma once

#include <string>
#include <sstream>
#include "tm_direction.hpp"

using std::string;
using std::stringstream;

class TuringMachineTransition
{
private:
	string source_state;
	char read_char;
	string dest_state;
	char write_char;
	direction move_dir;
public:
	TuringMachineTransition(string, char, string, char, direction);
	string getSourceState() const;
	char getReadCharacter() const;
	string getDestinationState() const;
	char getWriteCharacter() const;
	direction getMoveDirection() const;
	string toString() const;
};
