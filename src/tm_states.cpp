#include "tm_states.hpp"

// validates that state names are a combination of uppercase lowercase numbers and underscores
// validates that state names are unique
void TuringMachineStates::load(string def, bool& valid)
{
	vector<string> items = util::split(def, ' ');

	for(auto it = items.begin(); it != items.end(); ++it)
	{
		string item = *it;

		// check for invalid characters in state names
		if(!regex_match(item, regex("^[A-Za-z0-9_]+$")))
		{
			cout << "State name '" << item << "' may only contain letters, numbers and underscores!" << endl;
			valid = false;
		}

		// check for duplicate state names
		else if(contains(item))
		{
			cout << "State list contains duplicate state '" << item << "'!" << endl;
			valid = false;
		}
		else
			states.insert(item);
	}
}

// validates that the list of states is a subset of the given list of states
void TuringMachineStates::validate(const TuringMachineStates& superset, bool& valid) const
{
	for(auto it = states.begin(); it != states.end(); ++it)
	{
		if(!superset.contains(*it))
		{
			cout << "Final states contains state '" << *it << "' not defined in states!" << endl;
			valid = false;
		}
	}
}

bool TuringMachineStates::contains(string state) const
{
	return states.find(state) != states.end();
}

string TuringMachineStates::toString() const
{
	return "{" + util::join(states, ", ") + "}";
}
