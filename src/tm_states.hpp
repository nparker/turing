#pragma once

#include <iostream>
#include <set>
#include <regex>
#include "util.hpp"

using std::cout;
using std::endl;
using std::string;
using std::set;
using std::vector;
using std::regex;

class TuringMachineStates
{
private:
	set<string> states;
public:
	void load(string, bool&);
	void validate(const TuringMachineStates&, bool&) const;
	bool contains(string) const;
	string toString() const;
};
