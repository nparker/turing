#include "tm_transfunc.hpp"

// validates that transitions are completely specified
// validates that states have valid names
// validates that characters are single characters
// validates that characters are not reserved
// validates that directions are single characters
// validates that directions are either left or right
// validates that at most one transition exists for a given source state and read char
void TuringMachineTransitionFunctions::load(string def, bool& valid)
{
	vector<string> items = util::split(def, ' ');

	TuringMachineTransition* t;

	int i = 0;

	while(i < items.size())
	{
		// check that transition is completely defined
		if(items.size() - i < 5)
		{
			cout << "One or more components of a transition function is missing!" << endl;
			break;
		}

		bool transition_valid = true;

		string source_state = items[i++];

		// check that source state name is valid
		if(!regex_match(source_state, regex("^[A-Za-z0-9_]+$")))
		{
			cout << "Source state name '" << source_state 
				<< "' may only contain letters, numbers and underscores!" << endl;
			transition_valid = false;
		}

		string read_char_str = items[i++];

		// check that read char is only one character
		if(read_char_str.length() > 1)
		{
			cout << "Read character '" << read_char_str << "' cannot be more than one character!" << endl;
			transition_valid = false;
		}

		char read_char = read_char_str[0];

		// check that read char is not a reserved character
		if(read_char == '[' || read_char == ']' || read_char == '<' 
			|| read_char == '>' || read_char == '\\')
		{
			cout << "Read character cannot contain reserved character '" << read_char << "'!" << endl;
			transition_valid = false;
		}

		string dest_state = items[i++];

		// check that dest state name is valid
		if(!regex_match(dest_state, regex("^[A-Za-z0-9_]+$")))
		{
			cout << "Destination state name '" << dest_state 
				<< "' may only contain letters, numbers and underscores!" << endl;
			transition_valid = false;
		}

		string write_char_str = items[i++];

		// check that write char is only one character
		if(write_char_str.length() > 1)
		{
			cout << "Write character '" << write_char_str << "' cannot be more than one character!" << endl;
			transition_valid = false;
		}

		char write_char = write_char_str[0];
		
		// check that write char is not a reserved character
		if(write_char == '[' || write_char == ']' || write_char == '<' 
			|| write_char == '>' || write_char == '\\')
		{
			cout << "Write character cannot contain reserved character '" << write_char << "'!" << endl;
			transition_valid = false;
		}

		string move_dir_str = items[i++];
		
		// check that move direction is only one character
		if(move_dir_str.length() > 1)
		{
			cout << "Move direction '" << move_dir_str << "' cannot be more than one character!" << endl;
			transition_valid = false;
		}

		char move_dir_ch = move_dir_str[0];
		direction move_dir;

		// check that move direction is either L or R
		if(move_dir_ch == 'l' || move_dir_ch == 'L')
			move_dir = left;
		else if(move_dir_ch == 'r' || move_dir_ch == 'R')
			move_dir = right;
		else
		{
			cout << "Move direction '" << move_dir_ch << "' is invalid - can only be L or R!" << endl;
			transition_valid = false;
		}

		bool found = false;
		find(source_state, read_char, dest_state, write_char, move_dir, found);

		// check that a transition is not already defined for this state and char
		if(found)
		{
			cout << "Only one transition for source state '" << source_state << "' and read character '" 
				<< read_char << "' may be defined!" << endl;
			transition_valid = false;
		}

		if(transition_valid)
		{
			t = new TuringMachineTransition(source_state, read_char, dest_state, write_char, move_dir);
			transitions.push_back(*t);
		}
		else
			valid = false;
	}
}

// validates that read/write characters are in tape alphabet
// validates that source/dest states are in states
// validates that source states are not in final states
void TuringMachineTransitionFunctions::validate(const TuringMachineAlphabet& tape_alphabet, 
	const TuringMachineStates& states, const TuringMachineStates& final_states, bool& valid) const
{
	for(auto it = transitions.begin(); it != transitions.end(); ++it)
	{
		TuringMachineTransition t = *it;

		// check that read/write characters exist in tape alphabet
		if(!tape_alphabet.contains(t.getReadCharacter()))
		{
			cout << "Transition read character '" << t.getReadCharacter() 
				<< "' is not defined in the tape alphabet!" << endl;
			valid = false;
		}

		if(!tape_alphabet.contains(t.getWriteCharacter()))
		{
			cout << "Transition write character '" << t.getWriteCharacter() 
				<< "' is not defined in the tape alphabet!" << endl;
			valid = false;
		}

		// check that source/dest states exist in states
		if(!states.contains(t.getSourceState()))
		{
			cout << "Transition source state '" << t.getSourceState() 
				<< "' is not a defined state!" << endl;
			valid = false;
		}

		if(!states.contains(t.getDestinationState()))
		{
			cout << "Transition destination state '" << t.getDestinationState() 
				<< "' is not a defined state!" << endl;
			valid = false;
		}

		// check that source state does not exist in final states
		if(final_states.contains(t.getSourceState()))
		{
			cout << "Transition source state '" << t.getSourceState() 
				<< "' is defined to be a final state so is invalid!" << endl;
			valid = false;
		}
	}
}

void TuringMachineTransitionFunctions::find(string source_state, char read_char, 
	string& dest_state, char& write_char, direction& move_dir, bool& found) const
{
	for(auto it = transitions.begin(); it != transitions.end(); ++it)
	{
		TuringMachineTransition t = *it;
		if(t.getSourceState() == source_state && t.getReadCharacter() == read_char)
		{
			dest_state = t.getDestinationState();
			write_char = t.getWriteCharacter();
			move_dir = t.getMoveDirection();
			found = true;
			break;
		}
	}
}

void TuringMachineTransitionFunctions::viewDefinition() const
{
	for(auto it = transitions.begin(); it != transitions.end(); ++it)
		cout << " \u03B4" << (*it).toString() << endl;
}
