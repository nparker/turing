#include "tm_alphabet.hpp"

// loads characters from the given chunk of the definition file
// validates that alphabet is not empty
// validates that characters are single characters
// validates that characters are not reserved characters
// validates that characters are unique
void TuringMachineAlphabet::load(string def, bool& valid)
{
	vector<string> items = util::split(def, ' ');

	// check for empty alphabet
	if(items.begin() == items.end())
	{
		cout << "Alphabet cannot be empty!" << endl;
		valid = false;
	}
	else
	{
		for(auto it = items.begin(); it != items.end(); ++it)
		{
			string item = *it;

			// check for characters that aren't just a character
			if(item.length() > 1)
			{
				cout << "Alphabet character '" << item
					<< "' cannot be more than one character!" << endl;
				valid = false;
				continue;
			}

			char c = item[0];
			
			// check for reserved characters
			if(c == '[' || c == ']' || c == '<' || c == '>' || c == '\\')
			{
				cout << "Alphabet cannot contain reserved character '" << c << "'!" << endl;
				valid = false;
			}

			// check for duplicate characters
			else if(contains(c))
			{
				cout << "Alphabet contains duplicate character '" << c << "'!" << endl;
				valid = false;
			}
			else
				characters.insert(c);
		}
	}
}

// validates that the alphabet is a subset of the given alphabet
void TuringMachineAlphabet::validate(const TuringMachineAlphabet& superset, bool& valid) const
{
	for(auto it = characters.begin(); it != characters.end(); ++it)
		if(!superset.contains(*it))
		{
			cout << "Input alphabet contains character '" << *it 
				<< "' not defined in the tape alphabet!" << endl;
			valid = false;
		}
}

// checks whether the alphabet contains the given character
bool TuringMachineAlphabet::contains(char c) const
{
	return characters.find(c) != characters.end();
}

// formats the alphabet as a string for use with the view command
string TuringMachineAlphabet::toString() const
{
	return "{" + util::join(characters, ", ") + "}";
}
