#pragma once

#include <iostream>
#include <vector>
#include <set>
#include "util.hpp"

using std::cout;
using std::endl;
using std::string;
using std::set;
using std::vector;

class TuringMachineAlphabet
{
private:
	set<char> characters;
public:
	void load(string, bool&);
	void validate(const TuringMachineAlphabet&, bool&) const;
	bool contains(char) const;
	string toString() const;
};
