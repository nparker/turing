#pragma once

#include <string>
#include <sstream>
#include <vector>
#include <set>

using std::string;
using std::stringstream;
using std::vector;
using std::set;

class util
{
public:
	static string underline(string);
	static string join(vector<string>, string);
	static string join(set<string>, string);
	static string join(set<char>, string);
	static vector<string> split(string, char);
	static vector<string> split(string);
	static string center(string, int);
};
