#include "util.hpp"

string util::underline(string str)
{
	return "\33[4m" + str + "\33[0m";
}

string util::join(vector<string> strvec, string delim)
{
	string out = "";

	for(auto it = strvec.begin(); it != strvec.end(); ++it)
	{
		if(it != strvec.begin())
			out += delim;
		out += *it;
	}

	return out;
}

string util::join(set<string> strset, string delim)
{
	string out = "";

	for(auto it = strset.begin(); it != strset.end(); ++it)
	{
		if(it != strset.begin())
			out += delim;
		out += *it;
	}

	return out;
}

string util::join(set<char> charset, string delim)
{
	string out = "";

	for(auto it = charset.begin(); it != charset.end(); ++it)
	{
		if(it != charset.begin())
			out += delim;
		out += *it;
	}

	return out;
}

vector<string> util::split(string str, char delim)
{
	vector<string> results;
	string item;
	stringstream ss;
	ss.str(str);

	while(getline(ss, item, delim))
		results.push_back(item);

	return results;
}

vector<string> util::split(string str)
{
	vector<string> results;
	string item;
	stringstream ss;
	ss.str(str);

	while(getline(ss, item))
		results.push_back(item);

	return results;
}

string util::center(string str, int width)
{
	if(str.length() > width)
		return str;

	int padding = width - str.length();
	int left = padding / 2;
	int right = padding - left;

	return string(left, ' ') + str + string(right, ' ');
}
