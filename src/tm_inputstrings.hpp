#pragma once

#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include "tm.hpp"
#include "util.hpp"

using std::cout;
using std::endl;
using std::ifstream;
using std::ofstream;
using std::string;
using std::stringstream;
using std::vector;

class TuringMachineInputStrings
{
private:
	string filename;
	bool changed;
	vector<string> input_strings;
public:
	void load(string, const TuringMachine&);
	bool findInputString(int, string&) const;
	bool insertInputString(string);
	void removeInputString(int);
	void viewInputStrings() const;
	void writeInputStrings() const;
};
