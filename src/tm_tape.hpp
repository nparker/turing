#pragma once

#include <iostream>
#include "tm_alphabet.hpp"
#include "tm_direction.hpp"

using std::cout;
using std::endl;
using std::string;

class TuringMachineTape
{
private:
	string cells;
	size_t current_cell;
	char blank_character;
public:
	TuringMachineTape() : cells(""), current_cell(0), blank_character(' ') { };
	void load(string, bool&);
	void validate(const TuringMachineAlphabet&, const TuringMachineAlphabet&, bool&) const;
	void initialize(string);
	void update(char, direction);
	string getLeft(size_t) const;
	string getRight(size_t) const;
	char getBlankCharacter() const { return blank_character; }
	char getCurrentCharacter() const { return cells[current_cell]; }
	bool isFirstCell() const { return current_cell == 0; }
};
