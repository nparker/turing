#include "tm.hpp"

TuringMachine::TuringMachine() :
	valid(true), description(""), initial_state("")
{
	initialize("");
}

// splits definition file and sends each chunk to the appropriate component
// validates all components of turing machine definition
// validates that identifiers are present and in order
void TuringMachine::load(string filename)
{
	ifstream def(filename + ".def");

	string defstr;

	if(def.is_open())
	{
		stringstream ss;
		ss << def.rdbuf();
		def.close();

		defstr = ss.str();
	}
	else
	{
		cout << "Could not open file '" << filename << ".def'!" << endl;
		valid = false;
		return;
	}

	smatch match;

	string identifiers[] = {"states", "input_alphabet", "tape_alphabet", 
		"transition_function", "initial_state", "blank_character", 
		"final_states"};

	// check for missing identifiers and identifier order
	string defstr_copy = defstr;
	
	for(int i = 0; i < 7; i++)
	{
		if(!regex_search(defstr_copy, match, regex("\\s*" + identifiers[i] + ":\\s*", std::regex_constants::icase)))
		{
			cout << "Missing or misplaced identifier '" << identifiers[i] << "'!" << endl;
			valid = false;
		}
		else
			defstr_copy = match.suffix();
	}

	// attempt to extract info for each component
	if(regex_search(defstr, match, regex("^([\\s\\S]*)\\s*[^_]states:", std::regex_constants::icase)))
		description = match[1];

	description = regex_replace(description, regex("^\\s+|\\s+$"), " ");

	defstr = regex_replace(defstr, regex("\\s+"), " ");

	if(regex_search(defstr, match, regex("states:\\s(.*)\\sinput_alphabet:", std::regex_constants::icase)))
		states.load(match[1], valid);
	else
		cout << "Warning: States definition is empty!" << endl;

	if(regex_search(defstr, match, regex("input_alphabet:\\s(.*)\\stape_alphabet:", std::regex_constants::icase)))
		input_alphabet.load(match[1], valid);
	else
		cout << "Warning: Input alphabet is empty!" << endl;

	if(regex_search(defstr, match, regex("tape_alphabet:\\s(.*)\\stransition_function:", std::regex_constants::icase)))
		tape_alphabet.load(match[1], valid);
	else
		cout << "Warning: Tape alphabet is empty!" << endl;

	if(regex_search(defstr, match, regex("transition_function:\\s(.*)\\sinitial_state:", std::regex_constants::icase)))
		transition_function.load(match[1], valid);
	else
	{
		cout << "Transition function cannot be empty!" << endl;
		valid = false;
	}

	if(regex_search(defstr, match, regex("initial_state:\\s(.*)\\sblank_character:", std::regex_constants::icase)))
		initial_state = match[1];

	if(regex_search(defstr, match, regex("blank_character:\\s(.*)\\sfinal_states:", std::regex_constants::icase)))
		tape.load(match[1], valid);
	else
	{
		cout << "Blank character cannot be empty!" << endl;
		valid = false;
	}

	if(regex_search(defstr, match, regex("final_states:\\s(.*)$", std::regex_constants::icase)))
		final_states.load(match[1], valid);
	else
		cout << "Warning: No final state is defined!" << endl;

	// validate input alphabet is subset of tape alphabet
	input_alphabet.validate(tape_alphabet, valid);

	// validate final states is subset of states
	final_states.validate(states, valid);

	// validate blank character is in tape alphabet but not input alphabet
	tape.validate(tape_alphabet, input_alphabet, valid);

	// validate transition states/chars/directions
	transition_function.validate(tape_alphabet, states, final_states, valid);

	// validate initial state is not empty
	if(initial_state == "")
	{
		cout << "Initial state cannot be empty!" << endl;
		valid = false;
	}

	// validate initial state is defined state
	else if(!states.contains(initial_state))
	{
		cout << "Initial state '" << initial_state << "' is not a defined state!" << endl;
		valid = false;
	}
}

void TuringMachine::viewDefinition() const
{
	cout << description;

	cout << "\n\n Q = " << states.toString();
	cout << "\n\n \u03A3 = " << input_alphabet.toString();
	cout << "\n\n \u0393 = " << tape_alphabet.toString() << "\n\n";
	
	transition_function.viewDefinition();

	cout << "\n q\u2080 = " << initial_state;
	cout << "\n\n B = " << tape.getBlankCharacter();
	cout << "\n\n F = " << final_states.toString() << endl;
}

void TuringMachine::viewInstantaneousDescription(size_t max_cells) const
{
	cout << to_string(transitions_performed) << ". " << tape.getLeft(max_cells)
		<< "[" << current_state << "]" << tape.getRight(max_cells) << endl;
}

void TuringMachine::initialize(string input_string)
{
	original_input_string = input_string;
	if(original_input_string == "") original_input_string = "\\";
	tape.initialize(input_string);
	current_state = initial_state;
	has_run = false;
	is_running = false;
	accepted = false;
	rejected = false;
	transitions_performed = 0;
}

void TuringMachine::performTransitions(size_t max_transitions)
{
	has_run = true;
	is_running = true;

	char read_char, write_char;
	string dest_state;
	direction move_dir;

	bool found = false;

	for(size_t i = 0; i < max_transitions; ++i)
	{
		found = false;

		// are we done?
		if(final_states.contains(current_state))
		{
			is_running = false;
			accepted = true;
			cout << "Input string '" << original_input_string << "' was accepted in " 
				<< transitions_performed << " transitions!" << endl;
			break;
		}

		// read the current cell
		read_char = tape.getCurrentCharacter();

		// is a transition from this state and character possible?
		transition_function.find(current_state, read_char, dest_state, write_char, move_dir, found);

		// if no transition is possible then reject the input string
		// if a transition attempts to move left of the first cell then reject the input string
		if(!found || (tape.isFirstCell() && move_dir == left))
		{
			is_running = false;
			rejected = true;
			cout << "Input string '" << original_input_string << "' was rejected in "
				<< transitions_performed << " transitions!" << endl;
			break;
		}

		// do the transition
		try
		{
			tape.update(write_char, move_dir);
			current_state = dest_state;
			transitions_performed += 1;
		}
		catch(std::bad_alloc& e)
		{
			cout << "Error allocating tape memory! Terminating operation..." << endl;
			is_running = false;
			break;
		}
	}
}

void TuringMachine::terminate()
{
	if(!is_running)
		cout << "Turing machine is not running!" << endl;
	else
	{
		is_running = false;
		cout << "Input string '" << original_input_string << "' was neither accepted nor rejected in "
			<< transitions_performed << " transitions." << endl;
	}
}

// if the input string (as it appears in file or user input) is valid
// used only in loading from file and for validating user input
// validates that strings are composed of characters from input alphabet
bool TuringMachine::isValidInputString(string str) const
{ 
	// empty string is not valid
	if(str == "")
		return false;

	// backslash on a line by itself represents the empty string and is valid
	if(str == "\\")
		return true;

	// check for characters not defined in input alphabet
	for(int i = 0; i < str.length(); ++i)
	{
		if(!input_alphabet.contains(str[i]))
		{
			cout << "Discarding input string '" << str << "' with character '" << str[i] 
				<< "' not defined in input alphabet..." << endl;
			return false;
		}
	}

	return true;
}
