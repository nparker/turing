#include "tm_transition.hpp"

TuringMachineTransition::TuringMachineTransition(string source, char read,
	string dest, char write, direction dir) :
	source_state(source), read_char(read),
	dest_state(dest), write_char(write), move_dir(dir) { }

string TuringMachineTransition::getSourceState() const
{
	return source_state;
}

char TuringMachineTransition::getReadCharacter() const
{
	return read_char;
}

string TuringMachineTransition::getDestinationState() const
{
	return dest_state;
}

char TuringMachineTransition::getWriteCharacter() const
{
	return write_char;
}

direction TuringMachineTransition::getMoveDirection() const
{
	return move_dir;
}

string TuringMachineTransition::toString() const
{
	stringstream ss;
	ss << "(" << source_state << ", " << read_char << ") = (" << dest_state << ", " << write_char << ", " 
		<< (move_dir == left ? "L" : (move_dir == right ? "R" : "?")) << ")";
	return ss.str();
}
