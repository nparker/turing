#include "tm_driver.hpp"

void TuringMachineDriver::cmd_delete()
{
	string input = prompt("Enter an input string number: ");

	if(input == "") return;
	
	stringstream ss(input);
	int index;

	if(ss >> index && to_string(index) == input)
		is.removeInputString(index);
	else
		cout << "Invalid number entered!" << endl;
}

void TuringMachineDriver::cmd_exit()
{
	is.writeInputStrings();
}

void TuringMachineDriver::cmd_help()
{
	cout << " " << util::underline("D") << "elete\t\tDelete input string from list\n"
		<< " E" << util::underline("x") << "it\t\tExit application\n"
		<< " " << util::underline("H") << "elp\t\tHelp user\n"
		<< " " << util::underline("I") << "nsert\t\tInsert input string into list\n"
		<< " " << util::underline("L") << "ist\t\tList input strings\n"
		<< " " << util::underline("Q") << "uit\t\tQuit operation of Turing machine on input string\n"
		<< " " << util::underline("R") << "un\t\tRun Turing machine on input string\n"
		<< " S" << util::underline("e") << "t\t\tSet maximum number of transitions to perform\n"
		<< " Sho" << util::underline("w") << "\t\tShow status of application\n"
		<< " " << util::underline("T") << "runcate\tTruncate instantaneous descriptions\n"
		<< " " << util::underline("V") << "iew\t\tView Turing machine" << endl;
}

void TuringMachineDriver::cmd_insert()
{
	string input = prompt("Enter an input string: ");
	
	if(tm.isValidInputString(input) && is.insertInputString(input))
		cout << "Input string added!" << endl;
	
}

void TuringMachineDriver::cmd_run()
{
	if(tm.isRunning())
	{
		tm.performTransitions(max_transitions);
		tm.viewInstantaneousDescription(max_cells);
	}
	else
	{
		string input = prompt("Enter an input string number: ");
		
		if(input == "") return;

		stringstream ss(input);
		int index;

		if(ss >> index && to_string(index) == input && is.findInputString(index, input))
		{
			tm.initialize(input);
			tm.viewInstantaneousDescription(max_cells);
			tm.performTransitions(max_transitions);
			tm.viewInstantaneousDescription(max_cells);
		}
		else
			cout << "Invalid input string index!" << endl;
	}
}

void TuringMachineDriver::cmd_set()
{
	string input = prompt("Enter new maximum transitions [" + to_string(max_transitions) + "]: ");

	if(input == "") return;

	stringstream ss(input);
	size_t new_max;

	if(ss >> new_max && to_string(new_max) == input)
	{
		if(new_max != max_transitions)
		{
			max_transitions = new_max;
			cout << "Maximum transitions changed to " << new_max << "!" << endl;
		}
	}
	else
		cout << "Invalid number entered!" << endl;
}

void TuringMachineDriver::cmd_show()
{
	cout << util::center("THE TURINATOR v." + VERSION, WIDTH) << endl
		 << string(WIDTH, '-') << endl
		 << util::center("by Neil Parker", WIDTH) << endl
		 << util::center("CS 322 Software Engineering", WIDTH) << endl
		 << util::center("Neil Corrigan", WIDTH) << endl
		 << util::center("Spring 2017", WIDTH) << endl
		 << string(WIDTH, '-') << endl
		 << util::center("Max cells setting: " + to_string(max_cells), WIDTH) << endl
		 << util::center("Max transitions setting: " + to_string(max_transitions), WIDTH) << endl
		 << string(WIDTH, '-') << endl
		 << util::center("Turing machine name: " + filename, WIDTH) << endl;

	if(tm.hasRun())
	{
		if(tm.isRunning())
			cout << util::center("Status: Running", WIDTH) << endl;
		else // tm has run but is not running
		{
			cout << util::center("Status: Completed", WIDTH) << endl;
			if(tm.isAccepted())
				cout << util::center("Result: Accepted", WIDTH) << endl;
			else if(tm.isRejected())
				cout << util::center("Result: Rejected", WIDTH) << endl;
			else
				cout << util::center("Result: Terminated by user", WIDTH) << endl;
		}
		cout << util::center("Input string: " + tm.getInputString(), WIDTH) << endl;
		cout << util::center("Transitions performed: " +
			to_string(tm.getTransitionsPerformed()), WIDTH) << endl;
	}
	else // tm has not run
		cout << util::center("Status: Not yet run", WIDTH) << endl;
}

void TuringMachineDriver::cmd_truncate()
{
	string input = prompt("Enter new maximum tape cells to display [" + to_string(max_cells) + "]: ");

	if(input == "") return;
	
	stringstream ss(input);
	size_t new_max;

	if(ss >> new_max && to_string(new_max) == input)
	{
		if(new_max != max_cells)
		{
			max_cells = new_max;
			cout << "Maximum cells changed to " << new_max << "!" << endl;
		}
	}
	else
		cout << "Invalid number entered!" << endl;
}

string TuringMachineDriver::prompt(string prompt) const
{
	string input = "";
	cout << prompt;
	getline(cin, input);
	cout << endl;
	return input;
}

void TuringMachineDriver::main()
{
	cout << util::center("THE TURINATOR v." + VERSION, WIDTH) << endl
		 << string(WIDTH, '-') << endl;

	tm.load(filename);
	is.load(filename, tm);

	if(!tm.isValid())
	{
		cout << "One or more errors were found in the definition file. Exiting..." << endl;
		return;
	}

	cout << "Turing machine loaded successfully!\n\nEnter 'h' to view available commands!" << endl;

	string cmd = "";

	while(true)
	{
		cmd = prompt("\nCommand: ");

		if(cmd.length() > 1)
		{
			cout << "Commands can only be single characters!" << endl;
			continue;
		}
		
		switch(tolower(cmd[0]))
		{
			case 'd':
				cmd_delete(); break;
			case 'h':
				cmd_help(); break;
			case 'i':
				cmd_insert(); break;
			case 'l':
				is.viewInputStrings(); break;
			case 'q':
				tm.terminate(); break;
			case 'r':
				cmd_run(); break;
			case 'e':
				cmd_set(); break;
			case 'w':
				cmd_show(); break;
			case 't':
				cmd_truncate(); break;
			case 'v':
				tm.viewDefinition(); break;
			case 'x':
				cmd_exit(); return;
			case 0: // nothing entered
				break;
			default:
				cout << "Invalid command!" << endl;
		}
	}
}
