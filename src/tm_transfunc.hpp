#pragma once

#include <iostream>
#include <vector>
#include <regex>
#include "util.hpp"
#include "tm_alphabet.hpp"
#include "tm_states.hpp"
#include "tm_transition.hpp"
#include "tm_direction.hpp"

using std::string;
using std::vector;
using std::cout;

class TuringMachineTransitionFunctions
{
private:
	vector<TuringMachineTransition> transitions;
public:
	void load(string, bool&);
	void validate(const TuringMachineAlphabet&, const TuringMachineStates&,
		const TuringMachineStates&, bool&) const;
	void find(string, char, string&, char&, direction&, bool&) const;
	void viewDefinition() const;
};
