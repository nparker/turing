#pragma once

#include <iostream>
#include <fstream>
#include <sstream>
#include <regex>
#include "tm_alphabet.hpp"
#include "tm_states.hpp"
#include "tm_transfunc.hpp"
#include "tm_tape.hpp"

using std::cout;
using std::endl;
using std::string;
using std::stringstream;
using std::ifstream;
using std::regex;
using std::smatch;
using std::to_string;

class TuringMachine
{
private:
	string description;
	string initial_state;
	string current_state;
	string original_input_string;
	size_t transitions_performed;
	bool valid;
	bool has_run;
	bool is_running;
	bool accepted;
	bool rejected;
	
	TuringMachineAlphabet input_alphabet;
	TuringMachineAlphabet tape_alphabet;
	TuringMachineTransitionFunctions transition_function;
	TuringMachineTape tape;
	TuringMachineStates states;
	TuringMachineStates final_states;

public:
	TuringMachine();
	void load(string);
	void viewDefinition() const;
	void viewInstantaneousDescription(size_t) const;
	void initialize(string);
	void performTransitions(size_t);
	void terminate();
	bool isValidInputString(string) const;
	string getInputString() const { return original_input_string; }
	size_t getTransitionsPerformed() const { return transitions_performed; }
	bool isValid() const { return valid; }
	bool hasRun() const { return has_run; }
	bool isRunning() const { return is_running; }
	bool isAccepted() const { return accepted; }
	bool isRejected() const { return rejected; }
};
