#include <iostream>
#include "tm_driver.hpp"
#include "util.hpp"

using std::cout;
using std::endl;

int main(int argc, char** argv)
{
	if(argc != 2)
	{
		cout << "Usage: tm <name>" << endl;
		return 1;
	}

	TuringMachineDriver driver(argv[1]);
	driver.main();

	return 0;
}
