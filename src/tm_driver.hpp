#pragma once

#include <iostream>
#include "tm.hpp"
#include "tm_inputstrings.hpp"
#include "util.hpp"

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::stringstream;
using std::to_string;

class TuringMachineDriver
{
private:
	const string VERSION = "1.0";
	const int WIDTH = 45;

	TuringMachine tm;
	TuringMachineInputStrings is;

	string filename;
	size_t max_transitions;
	size_t max_cells;

	void cmd_delete();
	void cmd_exit();
	void cmd_help();
	void cmd_insert();
	void cmd_run();
	void cmd_set();
	void cmd_show();
	void cmd_truncate();
	string prompt(string) const;
public:
	TuringMachineDriver(string name) : filename(name), max_transitions(1), max_cells(32){ };
	void main();
};
