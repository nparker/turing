#include "tm_tape.hpp"

// loads blank character from given chunk of the definition file
// validates that the blank character is not empty
// validates that the blank character is only one character
// validates that the blank character is not a reserved character
void TuringMachineTape::load(string def, bool& valid)
{
	// check for empty string
	if(def == "")
	{
		cout << "Blank character cannot be empty!" << endl;
		valid = false;
	}

	// check for characters that aren't just a character
	else if(def.length() > 1)
	{
		cout << "Blank character cannot be more than one character!" << endl;
		valid = false;
	}

	char c = def[0];

	// check for reserved characters
	if(c == '[' || c == ']' || c == '<' || c == '>' || c == '\\')
	{
		cout << "Blank character cannot contain reserved character '" << c << "'!" << endl;
		valid = false;
	}
	else
		blank_character = c;
}

// validates that the blank character is in the tape alphabet
// validates that the blank character is not in the input alphabet
void TuringMachineTape::validate(const TuringMachineAlphabet& tape_alphabet, 
	const TuringMachineAlphabet& input_alphabet, bool& valid) const
{
	// check that the blank character is in the tape alphabet
	if(!tape_alphabet.contains(blank_character))
	{
		cout << "Tape alphabet must contain the blank character!" << endl;
		valid = false;
	}

	// check that the blank character is not in the input alphabet
	if(input_alphabet.contains(blank_character))
	{
		cout << "Input alphabet must not contain the blank character!" << endl;
		valid = false;
	}
}

void TuringMachineTape::initialize(string input_string)
{
	current_cell = 0;
	cells = input_string + blank_character;
}

void TuringMachineTape::update(char write_char, direction move_dir)
{
	if(current_cell == 0 && move_dir == left)
		return;

	cells[current_cell] = write_char;

	current_cell += move_dir;
	
	if(current_cell == cells.length())
		cells += blank_character;
}

string TuringMachineTape::getLeft(size_t max_cells) const
{
	if(current_cell > max_cells)
		return "<" + cells.substr(current_cell - max_cells, max_cells);
	else
		return cells.substr(0, current_cell);
}

string TuringMachineTape::getRight(size_t max_cells) const
{
	size_t end_cell = cells.find_last_not_of(blank_character) + 1;
	if(current_cell > end_cell)
		return "";
	else if(end_cell - current_cell > max_cells)
		return cells.substr(current_cell, max_cells) + ">";
	else
		return cells.substr(current_cell, end_cell - current_cell);
}
